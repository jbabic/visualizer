package org.powertac.visualizer;

import org.springframework.context.annotation.Bean;

import java.io.IOException;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;

/**
 * This class will configure web-related stuff. Namely, it will set-up a
 * resolver for WEBJARS, which makes the access of front-end libraries (e.g.,
 * jQuery) consistent and straightforward.
 * 
 * @author Jurica Babic
 *
 */
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter
{

  @Override
  public void addResourceHandlers (ResourceHandlerRegistry registry)
  {
    registry.addResourceHandler("/webjars/**")
            .addResourceLocations("classpath:/META-INF/resources/webjars/");

  }
  

  static class PairSerializer extends JsonSerializer<Pair>
  {

    @Override
    public void serialize (Pair pair, JsonGenerator jsonGenerator,
                           SerializerProvider serializerProvider)
                             throws IOException
    {
      jsonGenerator.writeStartArray(2);
      jsonGenerator.writeObject(pair.getLeft());
      jsonGenerator.writeObject(pair.getRight());
      jsonGenerator.writeEndArray();
    }
  }

  static class PairDeserializer extends JsonDeserializer<Pair>
  {

    @Override
    public Pair deserialize (JsonParser jsonParser,
                             DeserializationContext deserializationContext)
                               throws IOException
    {
      final Object[] array = jsonParser.readValueAs(Object[].class);
      return Pair.of(array[0], array[1]);
    }
  }

  /**
   * This class will turn a pair into JSON and vice versa.
   * http://docs.spring.io/spring-boot/docs/1.2.3.RELEASE/reference/htmlsingle/#
   * howto-customize-the-jackson-objectmapper
   * 
   * Another way to customize Jackson is to add beans of type
   * com.fasterxml.jackson.databind.Module to your context. They will be
   * registered with every bean of type ObjectMapper, providing a global
   * mechanism for contributing custom modules when you add new features to your
   * application.
   * 
   * @return
   */
  @Bean
  public Module pairJacksonModule ()
  {
    final SimpleModule module = new SimpleModule();
    module.addSerializer(Pair.class, new PairSerializer());
    module.addDeserializer(Pair.class, new PairDeserializer());
    return module;
  }
  
  @Bean
  public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
   MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
   ObjectMapper objectMapper = new ObjectMapper();
   objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
   jsonConverter.setObjectMapper(objectMapper);
   return jsonConverter;
  }

}
