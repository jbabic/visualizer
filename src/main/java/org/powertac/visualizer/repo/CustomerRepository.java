package org.powertac.visualizer.repo;

import java.util.List;

import org.powertac.visualizer.model.Competition;
import org.powertac.visualizer.model.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * A CRUD repository of {@link Customer} objects.
 * 
 * @author Jurica Babic
 *
 */
@RepositoryRestResource
public interface CustomerRepository
  extends PagingAndSortingRepository<Customer, Long>
{
    List<Customer> findByName (@Param("name") String name);

  Customer
    findByNameAndCompetition (@Param("name") String name,
                              @Param("competition") Competition competition);
  
  Customer findByIdCustomerInfoAndCompetition (long idCustomerInfo, Competition comp);

  List<Customer> findByPowerType (@Param("name") String name);

  List<Customer>
    findByPowerTypeAndCompetition (@Param("name") String name,
                                   @Param("competition") Competition competition);

  List<Customer>
    findByGenericPowerTypeAndCompetition (@Param("name") String genericPowerType,
                                          @Param("competition") Competition competition);
  
  List<Customer> findByCompetition (@Param("competition") Competition competition);

}
