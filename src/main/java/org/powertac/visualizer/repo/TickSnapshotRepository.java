package org.powertac.visualizer.repo;

import java.util.List;

import org.powertac.visualizer.model.Competition;
import org.powertac.visualizer.model.TickSnapshot;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface TickSnapshotRepository
  extends PagingAndSortingRepository<TickSnapshot, Long>
{
    List<TickSnapshot> findByCompetition (@Param("competition") Competition competition);
}
