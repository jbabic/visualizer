package org.powertac.visualizer.repo;

import java.util.List;

import org.powertac.visualizer.model.Broker;
import org.powertac.visualizer.model.Competition;
import org.powertac.visualizer.model.Tariff;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface TariffRepository
  extends PagingAndSortingRepository<Tariff, Long>
{

  public List<Tariff> findByCompetitionAndBroker (Competition competition,
                                                  Broker broker);

  public Tariff
    findByCompetitionAndTariffSpecId (@Param("competition") Competition comp,
                                      @Param("tariffSpecId") long tariffSpecId);

  public List<Tariff> findByCompetition (Competition competition);

  public List<Tariff>
    findByCompetitionAndActiveTrue (@Param("competition") Competition competition);

}
