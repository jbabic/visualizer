package org.powertac.visualizer.repo;

import org.powertac.visualizer.model.Competition;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * A CRUD repository of {@link Competition} objects.
 * 
 * @author Jurica Babic
 *
 */
public interface CompetitionRepository extends PagingAndSortingRepository<Competition, Long>
{
    public Competition findFirstByOrderByIdDesc ();
    
}
