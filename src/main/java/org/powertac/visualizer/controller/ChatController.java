package org.powertac.visualizer.controller;

import org.powertac.visualizer.domain.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class ChatController
{

  @MessageMapping("/chat")
  @SendTo("/topic/message")
  public Message sendMessage (Message message)
  {
    return message;
  }
}
