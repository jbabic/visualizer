package org.powertac.visualizer.controller;

import javax.validation.Valid;

import org.powertac.visualizer.domain.Game;
import org.powertac.visualizer.service.VisualizerCompetitionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * http://spring.io/guides/gs/validating-form-input/
 * 
 * @author Jurica Babic
 *
 */
@Controller
public class CompetitionManagerController extends WebMvcConfigurerAdapter
{

  @Autowired
  private VisualizerCompetitionManager manager;

  @Override
  public void addViewControllers (ViewControllerRegistry registry)
  {
    registry.addViewController("/results").setViewName("results");
  }

  @RequestMapping(value = "/manager", method = RequestMethod.GET)
  public String showForm (Game game)
  {
    return "form";
  }

  @RequestMapping(value = "/manager", method = RequestMethod.POST)
  public String checkGameParametersInfo (@Valid Game game,
                                         BindingResult bindingResult)
  {
    if (bindingResult.hasErrors()) {
      return "form";
    }
    if (game.getBootName().equalsIgnoreCase("sim")) {
      manager.runTestSimGame();
      System.out.println("We are going to run a sim game.");
    } else if(game.getBootName().equalsIgnoreCase("log")){
      manager.readStateLog();
      System.out.println("We are going to read a state log game.");
    }
    
    else {
      System.out.println("We are going to run a boot game.");
      manager.runTestBootGame();
    }
    return "redirect:/results";
  }

}
