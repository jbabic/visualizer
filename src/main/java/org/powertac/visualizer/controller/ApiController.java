package org.powertac.visualizer.controller;

import java.util.ArrayList;
import java.util.List;

import org.powertac.visualizer.domain.Message;
import org.powertac.visualizer.domain.MessageType;
import org.powertac.visualizer.model.Competition;
import org.powertac.visualizer.model.Control;
import org.powertac.visualizer.model.TickSnapshot;
import org.powertac.visualizer.repo.CompetitionRepository;
import org.powertac.visualizer.repo.ControlRepository;
import org.powertac.visualizer.repo.TickSnapshotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * A controller for the Visualizer API.
 * 
 * @author Jurica Babic
 *
 */
@Controller
public class ApiController
{
  @Autowired
  private CompetitionRepository competitionRepository;

  @Autowired
  private TickSnapshotRepository tickSnapshotRepository;

  @Autowired
  private ControlRepository controlRepository;

  @RequestMapping(value = "/api/retrieve-ticks-from-most-recent-game")
  public @ResponseBody List<TickSnapshot> retrieveTicksFromMostRecentGame ()
  {
    List<TickSnapshot> snaps = new ArrayList<TickSnapshot>();

    Competition comp = competitionRepository.findFirstByOrderByIdDesc();

    if (comp != null) {
      List<TickSnapshot> retrievedSnaps =
        tickSnapshotRepository.findByCompetition(comp);
      if (retrievedSnaps != null) {
        snaps.addAll(retrievedSnaps);
      }
    }

    return snaps;
  }

  @RequestMapping(value = "/api/retrieve-last-control")
  public @ResponseBody Message retrieveLastControl ()
  {

    Control control = controlRepository.findFirstByOrderByIdDesc();

    return new Message(MessageType.CONTROL, control);
  }

}
