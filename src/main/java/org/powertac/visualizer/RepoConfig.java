package org.powertac.visualizer;

import org.powertac.visualizer.model.TickSnapshot;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

public class RepoConfig extends RepositoryRestMvcConfiguration {
  
  @Override
  protected void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
      config.exposeIdsFor(TickSnapshot.class);
  }
}
