package org.powertac.visualizer.model;

import javax.persistence.Embeddable;

import org.powertac.visualizer.DoubleSerializer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;



/**
 * Copy of {@link org.powertac.samplebroker.PortfolioManagerService}.
 * 
 * Keeps track of customer status and usage. Usage is stored
 * per-customer-unit, but reported as the product of the per-customer
 * quantity and the subscribed population. This allows the broker to use
 * historical usage data as the subscribed population shifts.
 */
@Embeddable
@JsonInclude(Include.NON_DEFAULT)
public class RetailKPIHolder
{
  @JsonProperty("subCum")
  private int subscribedPopulationCum;
  @JsonSerialize(using = DoubleSerializer.class)
  private Double kwhCum = new Double(0);
  @JsonProperty("mCum")
  @JsonSerialize(using = DoubleSerializer.class)
  private Double moneyCum = new Double(0);
  
  @JsonProperty("sub")
  private int subscribedPopulation;
  @JsonSerialize(using = DoubleSerializer.class)
  private Double kwh = new Double(0);
  @JsonProperty("m")
  @JsonSerialize(using = DoubleSerializer.class)
  private Double money = new Double(0);
  
  @JsonProperty("rvkTxCum")
  private int revokedTariffsCum;
  @JsonProperty("pubTxCum")
  private int publishedTariffsCum;
  
  @JsonProperty("actTx")
  private int activeTariffs;
  
  @JsonProperty("rvkTx")
  private int revokedTariffs;
  @JsonProperty("pubTx")
  private int publishedTariffs;

  public RetailKPIHolder ()
  {
    super();
    

  }

  public RetailKPIHolder (RetailKPIHolder retailKPIHolder)
  {
    subscribedPopulationCum = retailKPIHolder.getSubscribedPopulationCum();
    kwhCum = retailKPIHolder.getKwhCum();
    moneyCum = retailKPIHolder.getMoneyCum();

    subscribedPopulation = retailKPIHolder.getSubscribedPopulation();
    kwh = retailKPIHolder.getKwh();
    money = retailKPIHolder.getMoney();

    revokedTariffsCum = retailKPIHolder.getRevokedTariffsCum();
    publishedTariffsCum = retailKPIHolder.getPublishedTariffsCum();

    activeTariffs = retailKPIHolder.getActiveTariffs();

    revokedTariffs = retailKPIHolder.getRevokedTariffs();
    publishedTariffs = retailKPIHolder.getPublishedTariffs();
  }

  // Adds new individuals to the count
  public void signup (int population)
  {
    subscribedPopulationCum += population;
    subscribedPopulation += population;
  }

  // Removes individuals from the count
  public void withdraw (int population)
  {
    subscribedPopulation -= population;
    subscribedPopulationCum -= population;
  }

  // Customer produces or consumes power. We assume the kwh value is negative
  // for production, positive for consumption
  public void produceConsume (double txKwh, double txMoney)
  {
    kwh += txKwh;
    money += txMoney;

    kwhCum += txKwh;
    moneyCum += txMoney;

    // log.debug("consume " + kwh + " at " + index + ", customer "
    // + customer.getName());
  }

  public void resetCurrentValues ()
  {
    subscribedPopulation = 0;
    kwh = new Double(0);
    money = new Double(0);
    revokedTariffs = 0;
    publishedTariffs = 0;
  }
  @JsonSerialize(using = DoubleSerializer.class)
  public double getKwh ()
  {
    return kwh;
  }

  public void incrementRevokedTariffs ()
  {
    revokedTariffs++;
    revokedTariffsCum++;
  }

  public void incrementPublishedTariffs ()
  {
    publishedTariffs++;
    publishedTariffsCum++;
  }

  public int getRevokedTariffsCum ()
  {
    return revokedTariffsCum;
  }

  public int getPublishedTariffsCum ()
  {
    return publishedTariffsCum;
  }

  public int getActiveTariffs ()
  {
    return activeTariffs;
  }

  public int getRevokedTariffs ()
  {
    return revokedTariffs;
  }

  public int getPublishedTariffs ()
  {
    return publishedTariffs;
  }

  public void setRevokedTariffsCum (int revokedTariffsCum)
  {
    this.revokedTariffsCum = revokedTariffsCum;
  }

  public void setPublishedTariffsCum (int publishedTariffsCum)
  {
    this.publishedTariffsCum = publishedTariffsCum;
  }

  public void setActiveTariffs (int activeTariffs)
  {
    this.activeTariffs = activeTariffs;
  }

  public void setRevokedTariffs (int revokedTariffs)
  {
    this.revokedTariffs = revokedTariffs;
  }

  public void setPublishedTariffs (int publishedTariffs)
  {
    this.publishedTariffs = publishedTariffs;
  }

  public Double getMoney ()
  {
    return money;
  }

  public Double getKwhCum ()
  {
    return kwhCum;
  }
  
  public Double getMoneyCum ()
  {
    return moneyCum;
  }

  public int getSubscribedPopulation ()
  {
    return subscribedPopulation;
  }

  public int getSubscribedPopulationCum ()
  {
    return subscribedPopulationCum;
  }

  public void setSubscribedPopulationCum (int subscribedPopulationCum)
  {
    this.subscribedPopulationCum = subscribedPopulationCum;
  }
  @JsonSerialize(using = DoubleSerializer.class)
  public void setKwhCum (Double kwhCum)
  {
    this.kwhCum = kwhCum;
  }
  @JsonSerialize(using = DoubleSerializer.class)
  public void setMoneyCum (Double moneyCum)
  {
    this.moneyCum = moneyCum;
  }

  public void setSubscribedPopulation (int subscribedPopulation)
  {
    this.subscribedPopulation = subscribedPopulation;
  }
  @JsonSerialize(using = DoubleSerializer.class)
  public void setKwh (Double kwh)
  {
    this.kwh = kwh;
  }
  @JsonSerialize(using = DoubleSerializer.class)
  public void setMoney (Double money)
  {
    this.money = money;
  }

  @Override
  public String toString ()
  {
    return "RetailKPIHolder [subscribedPopulationCum=" + subscribedPopulationCum
           + ", kwhCum=" + kwhCum + ", moneyCum=" + moneyCum
           + ", subscribedPopulation=" + subscribedPopulation + ", kwh=" + kwh
           + ", money=" + money + "]";
  }

  @Override
  public int hashCode ()
  {
    final int prime = 31;
    int result = 1;
    result = prime * result + activeTariffs;
    long temp;
    temp = Double.doubleToLongBits(kwh);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(kwhCum);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(money);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    temp = Double.doubleToLongBits(moneyCum);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    result = prime * result + publishedTariffs;
    result = prime * result + publishedTariffsCum;
    result = prime * result + revokedTariffs;
    result = prime * result + revokedTariffsCum;
    result = prime * result + subscribedPopulation;
    result = prime * result + subscribedPopulationCum;
    return result;
  }

  @Override
  public boolean equals (Object obj)
  {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    RetailKPIHolder other = (RetailKPIHolder) obj;
    if (activeTariffs != other.activeTariffs)
      return false;
    if (Double.doubleToLongBits(kwh) != Double.doubleToLongBits(other.kwh))
      return false;
    if (Double.doubleToLongBits(kwhCum) != Double
            .doubleToLongBits(other.kwhCum))
      return false;
    if (Double.doubleToLongBits(money) != Double.doubleToLongBits(other.money))
      return false;
    if (Double.doubleToLongBits(moneyCum) != Double
            .doubleToLongBits(other.moneyCum))
      return false;
    if (publishedTariffs != other.publishedTariffs)
      return false;
    if (publishedTariffsCum != other.publishedTariffsCum)
      return false;
    if (revokedTariffs != other.revokedTariffs)
      return false;
    if (revokedTariffsCum != other.revokedTariffsCum)
      return false;
    if (subscribedPopulation != other.subscribedPopulation)
      return false;
    if (subscribedPopulationCum != other.subscribedPopulationCum)
      return false;
    return true;
  }

}
