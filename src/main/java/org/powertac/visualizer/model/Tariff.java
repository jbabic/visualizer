package org.powertac.visualizer.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * 
 * Copy of {@link org.powertac.common.TariffSpecification}}
 * 
 * @author Jurica Babic
 *
 */
@Entity
public class Tariff
{

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "TARIFF_ID")
  private long id;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "COMPETITION_ID")
  private Competition competition;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "BROKER_ID")
  private Broker broker;

  private long tariffSpecId;

  private boolean active = true;

  /** Last date new subscriptions will be accepted. Null means never expire. */
  private Long expiration = null;

  /** Minimum contract duration (in milliseconds) */
  private long minDuration = 0l;

  /** Type of power covered by this tariff */
  private String powerType;

  /**
   * One-time payment for subscribing to tariff, positive for payment
   * to customer, negative for payment from customer.
   */
  private double signupPayment = 0.0;

  /**
   * Payment from customer to broker for canceling subscription before
   * minDuration has elapsed.
   */
  private double earlyWithdrawPayment = 0.0;

  /** Flat payment per period for two-part tariffs */
  private double periodicPayment = 0.0;

  @Embedded
  private RetailKPIHolder retailKPIHolder = new RetailKPIHolder();

  protected Tariff ()
  {
  }

  public Tariff (Competition competition, Broker broker, long tariffSpec,
                 Long expiration, long minDuration, String powerType,
                 double signupPayment, double earlyWithdrawPayment,
                 double periodicPayment)
  {
    super();
    this.competition = competition;
    this.broker = broker;
    this.tariffSpecId = tariffSpec;
    this.expiration = expiration;
    this.minDuration = minDuration;
    this.powerType = powerType;
    this.signupPayment = signupPayment;
    this.earlyWithdrawPayment = earlyWithdrawPayment;
    this.periodicPayment = periodicPayment;
  }

  public long getTariffSpecId ()
  {
    return tariffSpecId;
  }

  public void setActive (boolean active)
  {
    this.active = active;
  }

  public long getId ()
  {
    return id;
  }

  public boolean isActive ()
  {
    return active;
  }

  public RetailKPIHolder getRetailKPIHolder ()
  {
    return retailKPIHolder;
  }

  public void setRetailKPIHolder (RetailKPIHolder retailKPIHolder)
  {
    this.retailKPIHolder = retailKPIHolder;
  }

  public Competition getCompetition ()
  {
    return competition;
  }

  public Broker getBroker ()
  {
    return broker;
  }

  public Long getExpiration ()
  {
    return expiration;
  }

  public long getMinDuration ()
  {
    return minDuration;
  }

  public String getPowerType ()
  {
    return powerType;
  }

  public double getSignupPayment ()
  {
    return signupPayment;
  }

  public double getEarlyWithdrawPayment ()
  {
    return earlyWithdrawPayment;
  }

  public double getPeriodicPayment ()
  {
    return periodicPayment;
  }

}
