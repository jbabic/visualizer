package org.powertac.visualizer.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * 
 * This entity represents a certain customer population within Power TAC game.
 * 
 * @author Jurica Babic
 *
 */
@Entity
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Broker implements TimeslotCompleteActivator
{
  @Id
  @Column(name = "BROKER_ID")
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  /** Name of the broker */
  private String name;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "COMPETITION_ID")
  @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
  @JsonIdentityReference(alwaysAsId=true)
  private Competition competition;
  @Embedded
  private RetailKPIHolder retailKPIHolder = new RetailKPIHolder();

  private double cash = 0;

  protected Broker ()
  {

  }

  public RetailKPIHolder getRetailKPIHolder ()
  {
    return retailKPIHolder;
  }

  public void setRetailKPIHolder (RetailKPIHolder retailKPIHolder)
  {
    this.retailKPIHolder = retailKPIHolder;
  }

  public Broker (String name, Competition competition)
  {
    super();
    this.name = name;
    this.competition = competition;
  }

  public long getId ()
  {
    return id;
  }

  public String getName ()
  {
    return name;
  }

  public Competition getCompetition ()
  {
    return competition;
  }

  public double getCash ()
  {
    return cash;
  }

  public void setCash (double cash)
  {
    this.cash = cash;
  }

}
