package org.powertac.visualizer.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


/**
 * A model object used for storing per-tick (i.e., time slot) values. The
 * object is pushed to the front-end where the processing (e.g., assembly of
 * graphs) takes place.
 * 
 * @author Jurica Babic
 *
 */
@Entity
@JsonInclude(Include.NON_EMPTY)
public class TickSnapshot
{
  @Id
  @Column(name = "TICK_SNAP_ID")
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "COMPETITION_ID")
  @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
  @JsonIdentityReference(alwaysAsId=true)
  private Competition competition;

  /**
   * key: broker name; value: brokerTickValue
   */
  @ElementCollection(fetch = FetchType.LAZY)
  @JsonProperty("brokerTicks")
  private List<BrokerTickValue> brokerTickValues;

  @ElementCollection(fetch = FetchType.LAZY)
  @JsonProperty("customerTicks")
  private List<CustomerTickValue> customerTickValues;

  private long timeInstance;

  protected TickSnapshot ()
  {

  }

  public TickSnapshot (long timeInstance, Competition competition)
  {
    this.competition = competition;
    this.timeInstance = timeInstance;
    this.brokerTickValues = new ArrayList<BrokerTickValue>();
    this.customerTickValues = new ArrayList<CustomerTickValue>();

  }

  public List<BrokerTickValue> getBrokerTickValues ()
  {
    return brokerTickValues;
  }

  public Competition getCompetition ()
  {
    return competition;
  }

  public List<CustomerTickValue> getCustomerTickValues ()
  {
    return customerTickValues;
  }

  public long getTimeInstance ()
  {
    return timeInstance;
  }

  public long getId ()
  {
    return id;
  }
}
