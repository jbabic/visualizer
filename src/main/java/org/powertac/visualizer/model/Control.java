package org.powertac.visualizer.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.powertac.visualizer.domain.ControlInstruction;

/**
 * 
 * This entity represents a control instruction for a front-end.
 * 
 * @author Jurica Babic
 *
 */
@Entity
public class Control
{
  @Id
  @Column(name = "CONTROL_ID")
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @ManyToOne()
  @JoinColumn(name = "COMPETITION_ID")
  private Competition competition;
  
  private String instruction;

  protected Control ()
  {

  }


  public Control (Competition competition, ControlInstruction instruction)
  {
    super();
    this.competition = competition;
    this.instruction = instruction.toString();
  }
  
  public String getInstruction ()
  {
    return instruction;
  }

  public long getId ()
  {
    return id;
  }


  public Competition getCompetition ()
  {
    return competition;
  }
  
  
}
