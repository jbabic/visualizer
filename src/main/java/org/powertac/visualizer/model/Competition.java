package org.powertac.visualizer.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.joda.time.Instant;
import org.powertac.common.TimeService;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * An entity for the org.powertac.common.Competition.
 * 
 * @author Jurica Babic
 *
 */
@Entity
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Competition implements Comparable<Competition>
{
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  @Column(name = "COMPETITION_ID")
  private long id;

  private String status = GameStatus.INIT.toString();

  /**
   * Original ID from Power TAC.
   */
  private long idPowerTacCompetition;

  /** The competition's name */
  private String name;

  /** Optional text that further describes the competition */
  private String description;

  /** length of a timeslot in simulation minutes */
  private int timeslotLength;

  /** Number of timeslots in initialization data dump */
  private int bootstrapTimeslotCount; // 14 days

  /**
   * Number of extra timeslots at start of bootstrap before data collection
   * starts
   */
  private int bootstrapDiscardedTimeslots;

  /** Minimum number of timeslots, aka competition length */
  private int minimumTimeslotCount;

  private int expectedTimeslotCount;

  /**
   * concurrently open timeslots, i.e. time window in which broker actions like
   * trading are allowed
   */
  private int timeslotsOpen = 24;

  /**
   * # timeslots a timeslot gets deactivated ahead of the now timeslot (default:
   * 1 timeslot, which (given default length of 60 min) means that e.g. trading
   * is disabled 60 minutes ahead of time
   */
  private int deactivateTimeslotsAhead = 1;

  /** Minimum order quantity */
  private double minimumOrderQuantity; // MWh

  /** the start time of the simulation scenario, in sim time. */
  private Instant simulationBaseTime;

  /** timezone offset for scenario locale */
  private int timezoneOffset;

  /** approximate latitude in degrees north for scenario locale */
  private int latitude;

  /**
   * the time-compression ratio for the simulation. So if we are running
   * one-hour timeslots every 5 seconds, the rate would be 720 (=default).
   */
  private long simulationRate;

  /**
   * controls the values of simulation time values reported. If
   * we are running one-hour timeslots, then the modulo should be one hour,
   * expressed
   * in milliseconds. If we are running one-hour timeslots but want to update
   * time every
   * 30 minutes of simulated time, then the modulo would be 30*60*1000. Note
   * that
   * this will not work correctly unless the calls to updateTime() are made at
   * modulo/rate intervals. Also note that the reported time is computed as
   * rawTime - rawTime % modulo, which means it will never be ahead of the raw
   * simulation time. Note that values other than the length of a timeslot have
   * not been tested.
   */
  private long simulationModulo;

  @OneToMany(mappedBy = "competition", fetch=FetchType.LAZY)
  private List<Customer> customers;

  @OneToMany(mappedBy = "competition", fetch=FetchType.LAZY)
  private List<Broker> brokers;

  protected Competition ()
  {
  }

  public Competition (long idPowerTacCompetition, String name,
                      String description, int timeslotLength,
                      int bootstrapTimeslotCount,
                      int bootstrapDiscardedTimeslots, int minimumTimeslotCount,
                      int expectedTimeslotCount, int timeslotsOpen,
                      int deactivateTimeslotsAhead, double minimumOrderQuantity,
                      Instant simulationBaseTime, int timezoneOffset,
                      int latitude, long simulationRate, long simulationModulo)
  {
    super();
    this.idPowerTacCompetition = idPowerTacCompetition;
    this.name = name;
    this.description = description;
    this.timeslotLength = timeslotLength;
    this.bootstrapTimeslotCount = bootstrapTimeslotCount;
    this.bootstrapDiscardedTimeslots = bootstrapDiscardedTimeslots;
    this.minimumTimeslotCount = minimumTimeslotCount;
    this.expectedTimeslotCount = expectedTimeslotCount;
    this.timeslotsOpen = timeslotsOpen;
    this.deactivateTimeslotsAhead = deactivateTimeslotsAhead;
    this.minimumOrderQuantity = minimumOrderQuantity;
    this.simulationBaseTime = simulationBaseTime;
    this.timezoneOffset = timezoneOffset;
    this.latitude = latitude;
    this.simulationRate = simulationRate;
    this.simulationModulo = simulationModulo;
  }

  public String getStatus ()
  {
    return status;
  }

  public void setStatus (String status)
  {
    this.status = status;
  }

  public List<Broker> getBrokers ()
  {
    return brokers;
  }

  public List<Customer> getCustomers ()
  {
    return customers;
  }

  public void setCustomers (List<Customer> customers)
  {
    this.customers = customers;
  }

  public void setBrokers (List<Broker> brokers)
  {
    this.brokers = brokers;
  }

  public long getId ()
  {
    return id;
  }

  public long getIdPowerTacCompetition ()
  {
    return idPowerTacCompetition;
  }

  public long getTimeslotDuration ()
  {
    return timeslotLength * TimeService.MINUTE;
  }

  public String getName ()
  {
    return name;
  }

  public String getDescription ()
  {
    return description;
  }

  public int getTimeslotLength ()
  {
    return timeslotLength;
  }

  public int getBootstrapTimeslotCount ()
  {
    return bootstrapTimeslotCount;
  }

  public int getBootstrapDiscardedTimeslots ()
  {
    return bootstrapDiscardedTimeslots;
  }

  public int getMinimumTimeslotCount ()
  {
    return minimumTimeslotCount;
  }

  public int getExpectedTimeslotCount ()
  {
    return expectedTimeslotCount;
  }

  public int getTimeslotsOpen ()
  {
    return timeslotsOpen;
  }

  public int getDeactivateTimeslotsAhead ()
  {
    return deactivateTimeslotsAhead;
  }

  public double getMinimumOrderQuantity ()
  {
    return minimumOrderQuantity;
  }

  public Instant getSimulationBaseTime ()
  {
    return simulationBaseTime;
  }

  public int getTimezoneOffset ()
  {
    return timezoneOffset;
  }

  public int getLatitude ()
  {
    return latitude;
  }

  public long getSimulationRate ()
  {
    return simulationRate;
  }

  public long getSimulationModulo ()
  {
    return simulationModulo;
  }

  @Override
  public int compareTo (Competition o)
  {
    if (this.getId() < o.getId()) {
      return -1;
    }
    else if (this.getId() > o.getId()) {
      return 1;
    }
    return 0;
  }

}
