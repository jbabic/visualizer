package org.powertac.visualizer.model;

import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonProperty;

@Embeddable
public class CustomerTickValue
{

  private long id;
  @JsonProperty("retail")
  private RetailKPIHolder retailKPIHolder;

  protected CustomerTickValue ()
  {

  }

  public CustomerTickValue (long id, RetailKPIHolder retailKPIHolderCopy)
  {
    this.id = id;
    this.retailKPIHolder = retailKPIHolderCopy;
  }

  public RetailKPIHolder getRetailKPIHolder ()
  {
    return retailKPIHolder;
  }
  
  public long getId ()
  {
    return id;
  }

}
