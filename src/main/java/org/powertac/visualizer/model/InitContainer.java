package org.powertac.visualizer.model;

import java.util.List;

/**
 * Contains all the important initial meta stuff from a game, including info on
 * competition, brokers and customers.
 * 
 * @author Jurica Babic
 *
 */
public class InitContainer
{

  private Competition competition;
  private List<Broker> brokers;
  private List<Customer> customers;
  
  
  public InitContainer (Competition competition, List<Broker> brokers,
                        List<Customer> customers)
  {
    super();
    this.competition = competition;
    this.brokers = brokers;
    this.customers = customers;
  }
  
  

}
