package org.powertac.visualizer;

import java.awt.Desktop;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.powertac.visualizer.service.VisualizerMessageDispatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * 
 * This component will do a house keeping job on a startup, including
 * initialization of Initializable services (e.g., registrations for
 * handleMessage takes place here).
 * 
 * @author Jurica Babic
 *
 */
@Component
public class ApplicationStartup
  implements ApplicationListener<ContextRefreshedEvent>
{

  static private Logger log = Logger.getLogger(ApplicationStartup.class);

  @Autowired
  private VisualizerMessageDispatcher router;

  @Autowired
  private ApplicationContext context;

  @Override
  public void onApplicationEvent (ContextRefreshedEvent event)
  {
    // ADAPTED FROM: org.powertac.samplebroker.core.PowerTacBroker
    // initialize services
    Collection<Initializable> initializers =
      context.getBeansOfType(Initializable.class).values();
    for (Initializable svc: initializers) {
      svc.initialize();
      registerMessageHandlers(svc);
    }

  }

  /**
   * COPIED FROM: org.powertac.samplebroker.core.PowerTacBroker
   * 
   * Finds all the handleMessage() methdods and registers them.
   */
  private void registerMessageHandlers (Object thing)
  {
    Class<?> thingClass = thing.getClass();
    Method[] methods = thingClass.getMethods();
    for (Method method: methods) {
      if (method.getName().equals("handleMessage")) {
        Class<?>[] args = method.getParameterTypes();
        if (1 == args.length) {
          log.info("Register " + thing.getClass().getSimpleName()
                   + ".handleMessage(" + args[0].getSimpleName() + ")");
          router.registerMessageHandler(thing, args[0]);
        }
      }
    }
  }

}
