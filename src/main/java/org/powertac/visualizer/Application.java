package org.powertac.visualizer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * 
 * Entry point for the Visualizer.
 * 
 * @author Jurica Babic
 *
 */
@SpringBootApplication
@Configuration
@EnableAutoConfiguration(exclude={org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration.class})
public class Application
{

  public static void main (String[] args)
  {
    SpringApplication.run(Application.class, args);
  }
  

}
