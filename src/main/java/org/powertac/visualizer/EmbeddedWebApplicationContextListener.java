package org.powertac.visualizer;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.EmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerInitializedEvent;
import org.springframework.boot.context.embedded.EmbeddedWebApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EmbeddedWebApplicationContextListener
  implements ApplicationListener<EmbeddedServletContainerInitializedEvent>
{

  @Autowired
  EmbeddedWebApplicationContext webContext;

  @Override
  public void
    onApplicationEvent (EmbeddedServletContainerInitializedEvent event)
  {
    EmbeddedServletContainer servletContainer =
      webContext.getEmbeddedServletContainer();

    int port = servletContainer.getPort();
    String context = webContext.getServletContext().getContextPath();
    String host = "";
    try {
      host = InetAddress.getLocalHost().getHostAddress();
    }
    catch (UnknownHostException e) {
      e.printStackTrace();
    }

    System.out.println("Power TAC Visualizer: " + "http://" + host + ":" + port
                       + context);

  }

}
