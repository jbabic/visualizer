package org.powertac.visualizer.service;

import static org.powertac.util.MessageDispatcher.dispatch;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.powertac.common.interfaces.VisualizerMessageListener;
import org.powertac.logtool.common.NewObjectListener;
import org.springframework.stereotype.Service;

/**
 * 
 * ADAPTED FROM broker core
 * 
 * Routes incoming messages to broker components, and outgoing messages to the
 * server. Components must register for specific message types with the broker,
 * which passes the registrations to this router. For this to work, registered
 * components must implement a handleMessage(msg) method that takes the
 * specified type as its single argument.
 * 
 * @author Jurica Babic
 */
@Service
public class VisualizerMessageDispatcher implements VisualizerMessageListener, NewObjectListener
{
  static private Logger log =
    Logger.getLogger(VisualizerMessageDispatcher.class);

  private HashMap<Class<?>, Set<Object>> registrations;

  /**
   * Default constructor
   */
  public VisualizerMessageDispatcher ()
  {
    super();
    registrations = new HashMap<Class<?>, Set<Object>>();
  }

  // ------------- incoming messages ----------------
  /**
   * Sets up handlers for incoming messages by message type.
   */
  public void registerMessageHandler (Object handler, Class<?> messageType)
  {
    Set<Object> reg = registrations.get(messageType);
    if (reg == null) {
      reg = new HashSet<Object>();
      registrations.put(messageType, reg);
    }
    reg.add(handler);
  }

  // test-support
    Set<Object> getRegistrations (Class<?> messageType)
  {
    return registrations.get(messageType);
  }

  @Override
  public void receiveMessage (Object message)
  {
    
    Class<?> clazz = message.getClass();
    log.info("Message received " + clazz.getName());
    
    Set<Object> targets = registrations.get(clazz);
    if (targets == null) {
      log.warn("no targets for message of type " + clazz.getName());
      return;
    }
    for (Object target: targets) {
      dispatch(target, "handleMessage", message);
    }
    

  }
  
  /**
   * Dispatches a call to methodName inside target based on the type of message.
   * Allows polymorphic method dispatch without the use of visitor or double
   * dispatch schemes, which produce nasty couplings with domain types.
   * <p>
   * Note that this scheme finds only exact matches between types of arguments
   * and declared types of formal parameters for declared or inherited methods.
   * So it will not call a method with formal parameter types of 
   * (Transaction, List) if the actual arguments are (Transaction, ArrayList).
   * </p>
   */
  static public Object dispatch (Object target, 
                                 String methodName, 
                                 Object... args)
  {
    Logger log = Logger.getLogger(target.getClass().getName());
    Object result = null;
    try {
      Class[] classes = new Class[args.length];
      for (int index = 0; index < args.length; index++) {
        //log.debug("arg class: " + args[index].getClass().getName());
        classes[index] = (args[index].getClass());
      }
      // see if we can find the method directly
      Method method = target.getClass().getMethod(methodName, classes);
      log.debug("found method " + method);
      result = method.invoke(target, args);
    }
    catch (NoSuchMethodException nsm) {
      log.debug("Could not find exact match: " + nsm.toString());
    }
    catch (InvocationTargetException ite) {
      Throwable thr = ite.getTargetException();
      log.error("Cannot call " + methodName
                   + ": " + thr + "\n"
                   );      
    }
    catch (Exception ex) {
      log.error("Exception calling message processor: " + ex.toString());
    }
    return result;
  }
  
  

  @Override
  public void handleNewObject (Object obj)
  {
    receiveMessage(obj);
    
  }
}
