package org.powertac.visualizer.service;

import org.apache.log4j.Logger;
import org.powertac.common.msg.TimeslotUpdate;
import org.powertac.common.spring.SpringApplicationContext;
import org.powertac.logtool.LogtoolContext;
import org.powertac.logtool.common.DomainObjectReader;
import org.powertac.logtool.common.NewObjectListener;
import org.powertac.logtool.ifc.Analyzer;
import org.powertac.visualizer.model.Competition;

public class LogtoolExecutor extends LogtoolContext implements Analyzer
{
  static private Logger log = Logger.getLogger(LogtoolExecutor.class.getName());

  private DomainObjectReader dor;

  private NewObjectListener objListener;

  /**
   * Constructor does nothing. Call setup() before reading a file to
   * get this to work.
   */
  public LogtoolExecutor ()
  {
    super();
  }

  /**
   * Main method just creates an instance and passes command-line args to
   * its inherited cli() method.
   */
  // public static void main (String[] args)
  // {
  // new LogtoolExecutor().cli(args);
  // }

  /**
   * Takes two args, input filename and output filename
   */
  private void cli (String[] args)
  {
    if (args.length != 1) {
      System.out.println("Usage: <analyzer> input-file");
      return;
    }

    super.cli(args[0], this);
  }

  public void readLog (String logName, NewObjectListener listener)
  {
    objListener = listener;
    super.cli(logName, this);

  }

  /**
   * Creates data structures, opens output file. It would be nice to dump
   * the broker names at this point, but they are not known until we hit the
   * first timeslotUpdate while reading the file.
   */
  @Override
  public void setup ()
  {
    dor = (DomainObjectReader) SpringApplicationContext.getBean("reader");
    // brokerRepo = (BrokerRepo) SpringApplicationContext.getBean("brokerRepo");

    dor.registerNewObjectListener(new ObjectHandler(), null);

  }

  @Override
  public void report ()
  {
    System.out.println("Log complete");

  }

  class ObjectHandler implements NewObjectListener
  {

    private boolean ignore = true;

    @Override
    public void handleNewObject (Object thing)
    {
      if (ignore) {
        if (thing instanceof TimeslotUpdate) {
          // send competition
          objListener.handleNewObject(org.powertac.common.Competition.currentCompetition());
          ignore = false;
        }
        else {
          return;
        }
      }
      objListener.handleNewObject(thing);
    }

  }

}
