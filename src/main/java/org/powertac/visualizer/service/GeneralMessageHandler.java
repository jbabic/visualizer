package org.powertac.visualizer.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.joda.time.Instant;
import org.powertac.common.CashPosition;
import org.powertac.common.Competition;
import org.powertac.common.CustomerInfo;
import org.powertac.common.TariffSpecification;
import org.powertac.common.TariffTransaction;
import org.powertac.common.enumerations.PowerType;
import org.powertac.common.msg.CustomerBootstrapData;
import org.powertac.common.msg.SimEnd;
import org.powertac.common.msg.SimPause;
import org.powertac.common.msg.SimResume;
import org.powertac.common.msg.SimStart;
import org.powertac.common.msg.TariffRevoke;
import org.powertac.common.msg.TimeslotComplete;
import org.powertac.common.msg.TimeslotUpdate;
import org.powertac.visualizer.Initializable;
import org.powertac.visualizer.controller.ApiController;
import org.powertac.visualizer.domain.ControlInstruction;
import org.powertac.visualizer.model.Broker;
import org.powertac.visualizer.model.BrokerTickValue;
import org.powertac.visualizer.model.Control;
import org.powertac.visualizer.model.Customer;
import org.powertac.visualizer.model.CustomerTickValue;
import org.powertac.visualizer.model.GameStatus;
import org.powertac.visualizer.model.InitContainer;
import org.powertac.visualizer.model.RetailKPIHolder;
import org.powertac.visualizer.model.Tariff;
import org.powertac.visualizer.model.TickSnapshot;
import org.powertac.visualizer.repo.BrokerRepository;
import org.powertac.visualizer.repo.CompetitionRepository;
import org.powertac.visualizer.repo.ControlRepository;
import org.powertac.visualizer.repo.CustomerRepository;
import org.powertac.visualizer.repo.TariffRepository;
import org.powertac.visualizer.repo.TickSnapshotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Service;

/**
 * 
 * This is an implementation of the Initializable interface. It will be
 * initialized upon startup and registered for handling Power TAC messages in
 * case there are proper method signatures, e.g.,
 * "handleMessage(Competition competition)".
 * intended for the Visualizer.
 * 
 * @author Jurica Babic
 *
 */
@Service
public class GeneralMessageHandler implements Initializable
{
  static private Logger log = Logger.getLogger(GeneralMessageHandler.class);

  @Autowired
  private CompetitionRepository competitionRepository;

  @Autowired
  private BrokerRepository brokerRepository;

  @Autowired
  private CustomerRepository customerRepository;

  @Autowired
  private TickSnapshotRepository tickSnapshotRepository;

  @Autowired
  private TariffRepository tariffRepository;

  @Autowired
  ControlRepository controlRepository;

  @PersistenceContext(type = PersistenceContextType.EXTENDED)
  private EntityManager em;

  @Override
  public void initialize ()
  {

  }

  @Autowired
  public Pusher pusher;

  private org.powertac.visualizer.model.Competition currentCompetition;

  private boolean running;

  private int currentTimeslot = 0; // index of last started timeslot
  private int timeslotCompleted = 0; // index of last completed timeslot

  private Instant currentInstant;

  public void handleMessage (Competition c)
  {
    // create competition
    org.powertac.visualizer.model.Competition persistedCompetition =
      competitionRepository
              .save(new org.powertac.visualizer.model.Competition(c.getId(),
                                                                  c.getName(),
                                                                  c.getDescription(),
                                                                  c.getTimeslotLength(),
                                                                  c.getBootstrapTimeslotCount(),
                                                                  c.getBootstrapDiscardedTimeslots(),
                                                                  c.getMinimumTimeslotCount(),
                                                                  c.getExpectedTimeslotCount(),
                                                                  c.getTimeslotsOpen(),
                                                                  c.getDeactivateTimeslotsAhead(),
                                                                  c.getMinimumOrderQuantity(),
                                                                  c.getSimulationBaseTime(),
                                                                  c.getTimezoneOffset(),
                                                                  c.getLatitude(),
                                                                  c.getSimulationRate(),
                                                                  c.getSimulationModulo()));

    PowerType[] powertypeList =
      new PowerType[] { PowerType.CONSUMPTION, PowerType.PRODUCTION,
                        // STORAGE,
                        PowerType.INTERRUPTIBLE_CONSUMPTION,
                        PowerType.THERMAL_STORAGE_CONSUMPTION,
                        PowerType.SOLAR_PRODUCTION, PowerType.WIND_PRODUCTION,
                        PowerType.RUN_OF_RIVER_PRODUCTION,
                        PowerType.PUMPED_STORAGE_PRODUCTION,
                        PowerType.CHP_PRODUCTION, PowerType.FOSSIL_PRODUCTION,
                        PowerType.BATTERY_STORAGE, PowerType.ELECTRIC_VEHICLE };

    List<Broker> brokers = new ArrayList<Broker>();
    // create brokers and customerProfiles

    for (String n: c.getBrokers()) {
      Broker broker =
        brokerRepository.save(new Broker(n, persistedCompetition));
      brokers.add(broker);
    }
    ;

    List<Customer> customers = new ArrayList<>();

    // create customers:
    for (CustomerInfo ci: c.getCustomers()) {

      Customer savedCustomer = customerRepository
              .save(new Customer(ci.getId(), ci.getName(), ci.getPopulation(),
                                 ci.getPowerType().toString(),
                                 ci.getControllableKW(), ci.getUpRegulationKW(),
                                 ci.getDownRegulationKW(),
                                 ci.getStorageCapacity(),
                                 ci.isMultiContracting(), ci.isCanNegotiate(),
                                 ci.getPowerType().getGenericType().toString(),
                                 persistedCompetition));

      customers.add(savedCustomer);
    }

    persistedCompetition.setCustomers(customers);
    persistedCompetition.setBrokers(brokers);
    persistedCompetition = competitionRepository.save(persistedCompetition);
    // let us set brokers and customers once again to prevent lazy loading
    // exception when we attempt to push an initial control message:
    persistedCompetition.setCustomers(customers);
    persistedCompetition.setBrokers(brokers);

    // we will use that competition as the current one for all our queries
    currentCompetition = persistedCompetition;
    currentInstant = null;

  }

  /**
   * Receives the SimPause message, used to pause the clock.
   * While the clock is paused, the broker needs to ignore the local clock.
   */
  public void handleMessage (SimPause sp)
  {
    // local brokers can ignore this.
    // log.info("Paused at " + timeService.getCurrentDateTime().toString());
    // pausedAt = timeslotRepo.currentSerialNumber();
  }

  /**
   * Receives the SimResume message, used to update the clock.
   */
  public void handleMessage (SimResume sr)
  {
    // local brokers don't need to handle this
    log.info("Resumed");
    // pausedAt = 0;
    // timeService.setStart(sr.getStart().getMillis() - serverClockOffset);
    // timeService.updateTime();
  }

  private void updateCompetitionStatus (GameStatus status)
  {
    if (currentCompetition != null) {
      currentCompetition.setStatus(status.toString());
      currentCompetition = competitionRepository.save(currentCompetition);
    }
  }

  /**
   * Receives the SimStart message, used to start the clock. The
   * server's clock offset is subtracted from the start time indicated
   * by the server.
   */
  public void handleMessage (SimStart ss)
  {
    updateCompetitionStatus(GameStatus.RUNNING);

    log.info("SimStart - start time is " + ss.getStart().toString());
    // timeService.setStart(ss.getStart().getMillis() - serverClockOffset);
    // timeService.updateTime();
    // log.info("SimStart - clock set to "
    // + timeService.getCurrentDateTime().toString());
  }

  /**
   * Receives the SimEnd message, which ends the broker session.
   */
  public void handleMessage (SimEnd se)
  {
    log.info("SimEnd received");
    updateCompetitionStatus(GameStatus.END);

    running = false;
    notifyAll();
  }

  /**
   * Updates the sim clock on receipt of the TimeslotUpdate message,
   * which should be the first to arrive in each timeslot. We have to disable
   * all the timeslots prior to the first enabled slot, then create and enable
   * all the enabled slots.
   */
  public synchronized void handleMessage (TimeslotUpdate tu)
  {

    if (currentInstant == null) {
      // skip reporting on a very first timeslot update
      currentInstant = tu.getPostedTime();
      // but send a control message so the front-end can be initialized:

      Control control =
        new Control(currentCompetition, ControlInstruction.INIT);
      controlRepository.save(control);

      pusher.sendControlMessage(control);

      return;
    }

    currentInstant = tu.getPostedTime();
    perTimeslotUpdate();

  }

  /**
   * CashPosition is the last message sent by Accounting.
   * This is normally when any broker would submit its bids, so that's when
   * this Broker will do it.
   */
  public synchronized void handleMessage (TimeslotComplete tc)
  {
    if (tc.getTimeslotIndex() == currentTimeslot) {
      timeslotCompleted = currentTimeslot;
      notifyAll();
    }

    // perTimeslotUpdate();

  }

  private void perTimeslotUpdate ()
  {
    TickSnapshot ts =
      new TickSnapshot(currentInstant.getMillis(), currentCompetition);

    // reset per time slot KPI values:
    List<Broker> brokers =
      brokerRepository.findByCompetition(currentCompetition);
    List<Tariff> tariffs =
      tariffRepository.findByCompetition(currentCompetition);
    List<Customer> customers =
      customerRepository.findByCompetition(currentCompetition);

    for (Broker broker: brokers) {
      // make a broker tick value for a tick snapshot
      BrokerTickValue tv =
        new BrokerTickValue(broker.getId(), broker.getCash(),
                            new RetailKPIHolder(broker.getRetailKPIHolder()));
      ts.getBrokerTickValues().add(tv);
    }

    for (Broker broker: brokers) {

      // update model
      broker.getRetailKPIHolder().resetCurrentValues();
      brokerRepository.save(broker);
    }

    for (Tariff tariff: tariffs) {

      // update model
      tariff.getRetailKPIHolder().resetCurrentValues();
      tariffRepository.save(tariff);
    }
    for (Customer customer: customers) {
      // make a powertype and tick value for a tick snapshot
      CustomerTickValue tv = new CustomerTickValue(customer
              .getId(), new RetailKPIHolder(customer.getRetailKPIHolder()));
      ts.getCustomerTickValues().add(tv);

      // update model
      customer.getRetailKPIHolder().resetCurrentValues();
      customerRepository.save(customer);
    }

    for (Customer customer: customers) {
      // update model
      customer.getRetailKPIHolder().resetCurrentValues();
      customerRepository.save(customer);
    }

    tickSnapshotRepository.save(ts);

    pusher.sendTickSnapshotUpdates(ts);

  }

  public int getTimeslotCompleted ()
  {
    return timeslotCompleted;
  }

  public int getCurrentTimeslot ()
  {
    return currentTimeslot;
  }

  public void handleMessage (CustomerBootstrapData cbd)
  {

    Customer customer =
      customerRepository.findByNameAndCompetition(cbd.getCustomerName(),
                                                  currentCompetition);

    customer.setBootstrapNetUsage(Arrays.stream(cbd.getNetUsage()).boxed()
            .collect(Collectors.toList()));

    customerRepository.save(customer);

  }

  public void handleMessage (CashPosition cp)
  {
    org.powertac.common.Broker ptacBroker = cp.getBroker();

    // we only care about standard (retail+wholesale) brokers
    if (!ptacBroker.isWholesale()) {
      Broker broker =
        brokerRepository.findByCompetitionAndName(currentCompetition,
                                                  ptacBroker.getUsername());
      broker.setCash(cp.getBalance());
      brokerRepository.save(broker);
    }
  }

  /**
   * Handles a TariffSpecification. These are sent by the server when new
   * tariffs are
   * published. If it's not ours, then it's a competitor's tariff. We keep track
   * of
   * competing tariffs locally, and we also store them in the tariffRepo.
   */
  public synchronized void handleMessage (TariffSpecification spec)
  {
    org.powertac.common.Broker theBroker = spec.getBroker();

    Broker broker =
      brokerRepository.findByCompetitionAndName(currentCompetition,
                                                theBroker.getUsername());
    if (broker != null) {
      tariffRepository.save(new Tariff(currentCompetition, broker, spec.getId(),
                                       spec.getExpiration() == null? -1
                                                                   : spec.getExpiration()
                                                                           .getMillis(),
                                       spec.getMinDuration(),
                                       spec.getPowerType().toString(),
                                       spec.getSignupPayment(),
                                       spec.getEarlyWithdrawPayment(),
                                       spec.getPeriodicPayment()));
      // broker reporting:
      broker.getRetailKPIHolder().incrementPublishedTariffs();
      brokerRepository.save(broker);
    }
    else {
      log.error("Broker " + theBroker.getUsername() + " cannot be found.");
    }
  }

  /**
   * Handles a TariffTransaction. We only care about certain types: PRODUCE,
   * CONSUME, SIGNUP, and WITHDRAW.
   */
  public synchronized void handleMessage (TariffTransaction ttx)
  {
    try {

      // make sure we have this tariff
      TariffSpecification newSpec = ttx.getTariffSpec();
      if (newSpec == null) {
        log.error("TariffTransaction type=" + ttx.getTxType()
                  + " for unknown spec");
        System.err.println("TTX without tariff spec");
      }
      else {

        Tariff oldSpec =
          tariffRepository.findByCompetitionAndTariffSpecId(currentCompetition,
                                                            newSpec.getId());
        if (oldSpec == null) {
          log.error("Incoming spec " + newSpec.getId()
                    + " not matched in repo");
        }
      }
      TariffTransaction.Type txType = ttx.getTxType();

      ArrayList<RetailKPIHolder> retailKPIHolders = new ArrayList<>();

      Customer customer = null;
      if (ttx.getCustomerInfo() != null) {
        // customer retailKPI:
        customer = customerRepository.findByIdCustomerInfoAndCompetition(ttx
                .getCustomerInfo().getId(), currentCompetition);
      }
      if (customer != null) {
        retailKPIHolders.add(customer.getRetailKPIHolder());
      }
      Broker broker = brokerRepository
              .findByCompetitionAndName(currentCompetition,
                                        ttx.getBroker().getUsername());
      if (broker != null) {
        retailKPIHolders.add(broker.getRetailKPIHolder());
      }
      // tariff retailKPI:
      Tariff tariff = tariffRepository
              .findByCompetitionAndTariffSpecId(currentCompetition,
                                                ttx.getTariffSpec().getId());
      if (tariff != null) {
        retailKPIHolders.add(tariff.getRetailKPIHolder());
      }

      // update retailKPIHolders:
      for (RetailKPIHolder record: retailKPIHolders) {

        if (TariffTransaction.Type.SIGNUP == txType) {

          log.info("SIGNUP:" + ttx.toString() + "cnt_customers:"
                   + ttx.getCustomerCount());
          // keep track of customer counts
          record.signup(ttx.getCustomerCount());
        }
        else if (TariffTransaction.Type.WITHDRAW == txType) {
          log.info("WITHDRAW:" + ttx.toString() + "cnt_customers:"
                   + ttx.getCustomerCount());
          // customers presumably found a better deal
          record.withdraw(ttx.getCustomerCount());
        }
        else if (TariffTransaction.Type.PRODUCE == txType) {
          // if ttx count and subscribe population don't match, it will be hard
          // to estimate per-individual production
          if (ttx.getCustomerCount() != record.getSubscribedPopulation()) {
            log.warn("production by subset " + ttx.getCustomerCount()
                     + " of subscribed population "
                     + record.getSubscribedPopulation());
          }
          record.produceConsume(ttx.getKWh(), ttx.getCharge());
        }
        else if (TariffTransaction.Type.CONSUME == txType) {
          if (ttx.getCustomerCount() != record.getSubscribedPopulation()) {
            log.warn("consumption by subset " + ttx.getCustomerCount()
                     + " of subscribed population "
                     + record.getSubscribedPopulation());
          }
          record.produceConsume(ttx.getKWh(), ttx.getCharge());
        }
      }

      // TODO update domain objects:
      if (customer != null) {
        customerRepository.save(customer);
      }
      if (broker != null) {
        brokerRepository.save(broker);
      }
      if (tariff != null) {
        tariffRepository.save(tariff);
      }

    }
    catch (NullPointerException e) {
      StringBuilder stack = new StringBuilder();
      for (int i = 0; i < e.getStackTrace().length; i++) {
        stack.append(e.getStackTrace()[i]);
      }
      log.error("TariffTransaction NPE:" + e.getMessage() + " " + stack);
      ;
    }
  }

  /**
   * Handles a TariffRevoke message from the server, indicating that some
   * tariff has been revoked.
   */
  public synchronized void handleMessage (TariffRevoke tr)
  {
    log.info("Revoke tariff " + tr.getTariffId() + " from "
             + tr.getBroker().getUsername());
    Tariff tariff =
      tariffRepository.findByCompetitionAndTariffSpecId(currentCompetition,
                                                        tr.getTariffId());
    // tariffRepository.delete(tariff);
    tariff.setActive(false);
    tariffRepository.save(tariff);

    // broker reporting:
    Broker broker = tariff.getBroker();
    broker.getRetailKPIHolder().incrementRevokedTariffs();
    brokerRepository.save(broker);
  }

}
