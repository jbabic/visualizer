package org.powertac.visualizer.service;

import org.apache.log4j.Logger;
import org.powertac.visualizer.domain.ControlInstruction;
import org.powertac.visualizer.domain.Message;
import org.powertac.visualizer.domain.MessageType;
import org.powertac.visualizer.model.Control;
import org.powertac.visualizer.model.TickSnapshot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.broker.BrokerAvailabilityEvent;
import org.springframework.stereotype.Component;

@Component
public class Pusher implements ApplicationListener<BrokerAvailabilityEvent>
{
  private static final String TOPIC_MESSAGE = "/topic/message";

  static private Logger log = Logger.getLogger(GeneralMessageHandler.class);

  @Autowired
  private SimpMessagingTemplate messagingTemplate;

  @Override
  public void onApplicationEvent (final BrokerAvailabilityEvent event)
  {

  }

  public void sendTickSnapshotUpdates (TickSnapshot payload)
  {

    this.messagingTemplate
            .convertAndSend(TOPIC_MESSAGE,
                            new Message(MessageType.DATA, payload));

  }

  public void sendControlMessage (Control control)
  {
    this.messagingTemplate
            .convertAndSend(TOPIC_MESSAGE,
                            new Message(MessageType.CONTROL, control));

  }

}
