package org.powertac.visualizer.service;

import java.util.ArrayList;
import java.util.List;

import org.powertac.common.interfaces.CompetitionSetup;
import org.powertac.common.interfaces.VisualizerMessageListener;
import org.powertac.common.interfaces.VisualizerProxy;
import org.powertac.logtool.common.NewObjectListener;
import org.powertac.server.CompetitionSetupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

/**
 * This service runs Power TAC games (sim and boot).
 * 
 * @author Jurica Babic
 *
 */
@Service
public class VisualizerCompetitionManager
{

  private AbstractApplicationContext context;

  @Autowired
  private VisualizerMessageListener messageListener;

  @Autowired
  private NewObjectListener newObjectListener;

  public void runTestBootGame ()
  {

    // initialize and run
    if (null == context) {
      context = new ClassPathXmlApplicationContext("powertac.xml");
    }
    else {
      context.close();
      context.refresh();
      
    }
    context.registerShutdownHook();

    // configure a game:
    CompetitionSetup cs = context.getBean(CompetitionSetupService.class);
    cs.preGame();

    // register with a visualizer proxy in order to get messages:
    VisualizerProxy vp = context.getBean(VisualizerProxy.class);
    vp.registerVisualizerMessageListener(messageListener);

    // launch a boot session:
    cs.bootSession("dragan", null, null, null, null);

    System.out.println("We are out, game should be running! No blocking?");
  }

  public void runTestSimGame ()
  {
    // initialize and run
    if (null == context) {
      context = new ClassPathXmlApplicationContext("powertac.xml");
    }
    else {
      context.close();
      context.refresh();
    }
    context.registerShutdownHook();

    // configure a game:
    CompetitionSetup cs = context.getBean(CompetitionSetupService.class);
    cs.preGame();

    // register with a visualizer proxy in order to get messages:
    VisualizerProxy vp = context.getBean(VisualizerProxy.class);
    vp.registerVisualizerMessageListener(messageListener);

    List<String> brokers = new ArrayList<>();

    // launch a boot session:
    cs.simSession("dragan", null, null, null, brokers, null, null, null);
  }

  public void readStateLog ()
  {

    Thread session = new Thread() {
      @Override
      public void run ()
      {
        LogtoolExecutor le = new LogtoolExecutor();
        le.readLog("powertac-sim-7.state", newObjectListener);
      }
    };
    
    session.start();

  }

}
