package org.powertac.visualizer.domain;

public enum MessageType
{
 DATA, CONTROL, EMPTY
}
