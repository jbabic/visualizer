package org.powertac.visualizer;

import java.io.IOException;
import java.text.DecimalFormat;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * 
 * http://stackoverflow.com/questions/11520781/serialize-a-double-to-2-decimal-places-using-jackson
 * 
 * @author Jurica
 *
 */
public class DoubleSerializer extends JsonSerializer<Double> {
  @Override
  public void serialize(Double value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonGenerationException {
      if (null == value) {
          //write the word 'null' if there's no value available
          jgen.writeNull();
      } else {
          final String pattern = "0.##";
          //final String pattern = "###,###,##0.00";
          final DecimalFormat myFormatter = new DecimalFormat(pattern);
          final String output = myFormatter.format(value).replace(',', '.'); // force a dot notation for Javascript
          jgen.writeNumber(output);
      }
  }
}