package org.powertac.visualizer.repo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powertac.visualizer.Application;
import org.powertac.visualizer.model.Broker;
import org.powertac.visualizer.model.Competition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test for {@link BrokerRepository}.
 * 
 * @author Jurica Babic
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest
public class BrokerRepositoryTest
{

  @Autowired
  private BrokerRepository brokerRepository;

  @Autowired
  private CompetitionRepository competitionRepository;

  @Before
  public void beforeTests ()
  {
    brokerRepository.deleteAll();
    competitionRepository.deleteAll();
  }

  @Test
  public void testFindByCompetitionAndName ()
  {
    Competition competition = TestHelper.spawnCompetition();

    competitionRepository.save(competition);

    for (int i = 0; i < 10; i++) {
      competitionRepository.save(TestHelper.spawnRandomCompetition());
    }

    Broker broker = new Broker(TestHelper.BROKER_NAME, competition);
    Broker broker2 =
      new Broker(TestHelper.BROKER_NAME2, competition);
    brokerRepository.save(broker);
    brokerRepository.save(broker2);

    Broker retrievedBroker = brokerRepository
            .findByCompetitionAndName(competition, TestHelper.BROKER_NAME);

    assertEquals("Correct broker retrieved.", broker.getId(),
                 retrievedBroker.getId());
    assertTrue("Correct broker name.",
               broker.getName().equalsIgnoreCase(retrievedBroker.getName()));
  };

}
