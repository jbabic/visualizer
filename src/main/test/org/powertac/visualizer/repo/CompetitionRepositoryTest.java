package org.powertac.visualizer.repo;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powertac.visualizer.Application;
import org.powertac.visualizer.model.Competition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test for {@link CompetitionRepository}.
 * 
 * @author Jurica Babic
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest
public class CompetitionRepositoryTest
{

  @Autowired
  private CompetitionRepository competitionRepository;

  @Before
  public void beforeTests ()
  {
    competitionRepository.deleteAll();
  }

  @Test
  public void testFindFirstByOrderByIdDesc ()
  {
    assertNull("There is no competition in a repository.",
               competitionRepository.findFirstByOrderByIdDesc());

    for (int i = 0; i < 10; i++) {
      competitionRepository.save(TestHelper.spawnRandomCompetition());

    }
    // last saved competition
    Competition competition = TestHelper.spawnCompetition();
    competitionRepository.save(competition);

    // retrieve last competition
    Competition retrievedCompetition =
      competitionRepository.findFirstByOrderByIdDesc();

    assertNotNull("A competition is retrieved", retrievedCompetition);
    assertTrue("Correct competition name.", competition.getName()
            .equalsIgnoreCase(retrievedCompetition.getName()));

  };

}
