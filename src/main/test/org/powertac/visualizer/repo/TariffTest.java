package org.powertac.visualizer.repo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.joda.time.Instant;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powertac.common.CustomerInfo;
import org.powertac.common.TariffSpecification;
import org.powertac.common.TariffTransaction;
import org.powertac.common.enumerations.PowerType;
import org.powertac.visualizer.Application;
import org.powertac.visualizer.model.Broker;
import org.powertac.visualizer.model.Competition;
import org.powertac.visualizer.model.Customer;
import org.powertac.visualizer.model.RetailKPIHolder;
import org.powertac.visualizer.model.Tariff;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 
 * @author Jurica Babic
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest
public class TariffTest
{
  @Autowired
  private BrokerRepository brokerRepository;

  @Autowired
  private CompetitionRepository competitionRepository;

  @Autowired
  private CustomerRepository customerRepository;

  @Autowired
  private TariffRepository tariffRepository;

  @Before
  public void beforeTests ()
  {
    brokerRepository.deleteAll();
    competitionRepository.deleteAll();
    customerRepository.deleteAll();
    tariffRepository.deleteAll();
  }

  @Test
  public void brokerCompetitionRelationshipTest ()
  {
    Competition competition = TestHelper.spawnCompetition();
    competition = competitionRepository.save(competition);

    org.powertac.common.Broker ptacBroker =
      new org.powertac.common.Broker(TestHelper.BROKER_NAME);

    Broker broker = new Broker(ptacBroker.getUsername(), competition);
    broker = brokerRepository.save(broker);

    CustomerInfo cInfo = new CustomerInfo("customerInfo", 100);
    Customer customer =
      new Customer(cInfo.getId(), cInfo.getName(), cInfo.getPopulation(),
                   cInfo.getPowerType().toString(), cInfo.getControllableKW(),
                   cInfo.getUpRegulationKW(), cInfo.getDownRegulationKW(),
                   cInfo.getStorageCapacity(), cInfo.isMultiContracting(),
                   cInfo.isCanNegotiate(),
                   cInfo.getPowerType().getGenericType().toString(),
                   competition);
    customerRepository.save(customer);
    TariffSpecification tSpec =
      new TariffSpecification(ptacBroker, PowerType.CONSUMPTION);
    tSpec.withExpiration(new Instant());

    Tariff t =
      tariffRepository.save(new Tariff(competition, broker, tSpec.getId(),
                                       tSpec.getExpiration().getMillis(),
                                       tSpec.getMinDuration(),
                                       tSpec.getPowerType().toString(),
                                       tSpec.getSignupPayment(),
                                       tSpec.getEarlyWithdrawPayment(),
                                       tSpec.getPeriodicPayment()));

    TariffTransaction.Type type = TariffTransaction.Type.CONSUME;
    TariffTransaction ttx =
      new TariffTransaction(ptacBroker, 0, type, tSpec, cInfo, 100, 100d, 100d);

    // find a tariff of a tariff spec:
    Tariff tariff = tariffRepository
            .findByCompetitionAndTariffSpecId(competition,
                                              ttx.getTariffSpec().getId());

    assertNotNull("Tariff exists", tariff);

    assertEquals("Belongs to a broker.", tSpec.getBroker().getUsername(),
                 tariff.getBroker().getName());

    // process ttx for tariff, broker and customer:
    RetailKPIHolder tariffRetailKPI = tariff.getRetailKPIHolder();
    tariffRetailKPI.produceConsume(ttx.getKWh(), ttx.getCharge());

    Broker tariffBroker = tariff.getBroker();
    tariffBroker.getRetailKPIHolder().produceConsume(ttx.getKWh(),
                                                     ttx.getCharge());
    brokerRepository.save(tariffBroker);

    Customer retrievedCustomer = customerRepository
            .findByIdCustomerInfoAndCompetition(ttx.getCustomerInfo().getId(),
                                                competition);

    assertNotNull("Customer found based on customer info id from ttx",
                  retrievedCustomer);

    retrievedCustomer.getRetailKPIHolder().produceConsume(ttx.getKWh(),
                                                          ttx.getCharge());

    // persist, retrieve and test
    System.out.println("Not persisted tariff retail KPI."
                       + tariff.getRetailKPIHolder() + " TTX:" + ttx);
    // persist tariff, see if broker gets automatically:
    Tariff persistedTariff = tariffRepository.save(tariff);
    System.out.println("Persisted tariff retail KPI."
                       + persistedTariff.getRetailKPIHolder() + " TTX:" + ttx);
    assertTrue("Tariff retailKPI updated",
               ttx.getKWh() == persistedTariff.getRetailKPIHolder().getKwh());
    assertTrue("Broker retailKPI updated", ttx.getKWh() == persistedTariff
            .getBroker().getRetailKPIHolder().getKwh());

    // customer:
    Customer persistedCustomer = customerRepository.save(retrievedCustomer);

    assertTrue("Customer retailKPI updated",
               ttx.getKWh() == persistedCustomer.getRetailKPIHolder().getKwh());

  };

}
