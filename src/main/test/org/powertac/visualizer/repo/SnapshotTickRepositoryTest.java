package org.powertac.visualizer.repo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powertac.visualizer.Application;
import org.powertac.visualizer.model.Broker;
import org.powertac.visualizer.model.BrokerTickValue;
import org.powertac.visualizer.model.Competition;
import org.powertac.visualizer.model.RetailKPIHolder;
import org.powertac.visualizer.model.TickSnapshot;
import org.powertac.visualizer.repo.TestHelper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 * Test for {@link BrokerRepository}.
 * 
 * @author Jurica Babic
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest
public class SnapshotTickRepositoryTest
{

  @Autowired
  private BrokerRepository brokerRepository;

  @Autowired
  private CompetitionRepository competitionRepository;

  @Autowired
  private TickSnapshotRepository tsRepository;

  @Before
  public void beforeTests ()
  {
    brokerRepository.deleteAll();
    competitionRepository.deleteAll();
    tsRepository.deleteAll();
  }

  @Test
  public void testFindByCompetitionAndName ()
  {
    Competition competition = TestHelper.spawnCompetition();

    competitionRepository.save(competition);

    Broker broker = new Broker(TestHelper.BROKER_NAME, competition);
    Broker broker2 = new Broker(TestHelper.BROKER_NAME2, competition);

    broker.setCash(100);
    broker2.setCash(200);

    brokerRepository.save(broker);
    brokerRepository.save(broker2);

    TickSnapshot ts = new TickSnapshot(222, competition);

    for (Broker b: brokerRepository.findAll()) {
      BrokerTickValue btv =
        new BrokerTickValue(b.getId(), b.getCash(), new RetailKPIHolder());
      ts.getBrokerTickValues().add(btv);
    }

    tsRepository.save(ts);

    List<TickSnapshot> ticks = tsRepository.findByCompetition(competition);

    assertEquals("No of broker tick values", 2,
                 ticks.get(0).getBrokerTickValues().size());

  };

}
