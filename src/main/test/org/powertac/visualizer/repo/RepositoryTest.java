package org.powertac.visualizer.repo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powertac.visualizer.Application;
import org.powertac.visualizer.model.Broker;
import org.powertac.visualizer.model.Competition;
import org.powertac.visualizer.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Integration tests for {@link org.powertac.visualizer.repo.*}. Repositories
 * within that package are standard out-of-box CRUD implementations. We want to
 * test whether relationships among entities are correct (e.g., One-to-one and
 * Many-to-one)
 * 
 * @author Jurica Babic
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest
public class RepositoryTest
{
  @Autowired
  private BrokerRepository brokerRepository;

  @Autowired
  private CompetitionRepository competitionRepository;

  @Autowired
  private CustomerRepository customerRepository;

  @Before
  public void beforeTests ()
  {
    brokerRepository.deleteAll();
    competitionRepository.deleteAll();
    customerRepository.deleteAll();
  }

  @Test
  public void brokerCompetitionRelationshipTest ()
  {
    Competition competition = TestHelper.spawnCompetition();
    competitionRepository.save(competition);

    Broker broker = new Broker(TestHelper.BROKER_NAME, competition);
    Broker broker2 = new Broker(TestHelper.BROKER_NAME2, competition);
    brokerRepository.save(broker);
    brokerRepository.save(broker2);

    // get the broker and see whether there is a reference to a competition

    List<Broker> brokers = brokerRepository.findByName(TestHelper.BROKER_NAME);

    assertFalse("Resulting list is not empty.", brokers.isEmpty());

    assertEquals("Broker repo has exactly 2 brokers.", 2,
                 brokerRepository.count());

    Broker retrievedBroker = brokers.get(0);

    assertNotNull("Broker has a competition assigned",
                  retrievedBroker.getCompetition());

    assertEquals("Broker has a right competition assigned",
                 competition.getIdPowerTacCompetition(),
                 retrievedBroker.getCompetition().getIdPowerTacCompetition());

  };

  @Test
  public void customerCompetitionRelationshipTest ()
  {
    Competition competition = TestHelper.spawnCompetition();
    competitionRepository.save(competition);

    Customer customer =
      new Customer(TestHelper.CUSTOMER_POWERTAC_ID, TestHelper.CUSTOMER_NAME,
                   50, "", 0, 0, 0, 0, false, false,
                   TestHelper.GENERIC_POWER_TYPE, competition);
    customerRepository.save(customer);

    List<Customer> customers =
      customerRepository.findByName(TestHelper.CUSTOMER_NAME);

    assertFalse("Resulting list is not empty.", customers.isEmpty());

    Customer retrievedCustomer = customers.get(0);

    assertNotNull("Broker has a competition assigned",
                  retrievedCustomer.getCompetition());

    assertEquals("Broker has a right competition assigned",
                 competition.getIdPowerTacCompetition(),
                 retrievedCustomer.getCompetition().getIdPowerTacCompetition());

  }

}
