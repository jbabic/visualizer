package org.powertac.visualizer.repo;

import org.powertac.visualizer.model.Broker;
import org.powertac.visualizer.model.Competition;
import org.powertac.visualizer.model.Customer;

/**
 * A test helper with a set of handy methods.
 * 
 * @author Jurica Babic
 *
 */
public class TestHelper
{
  public static final String BROKER_NAME = "test-broker";
  public static final String BROKER_NAME2 = "test-broker2";
  public static final String COMPETITION_NAME = "test-competition";
  public static final String COMPETITION_DESC = "desc";
  public static final String CUSTOMER_NAME = "customer-name";
  public static final long CUSTOMER_POWERTAC_ID = 44;
  public static final String GENERIC_POWER_TYPE = "consumption";
  public static final String GENERIC_POWER_TYPE2 = "production";

  public static Competition spawnCompetition ()
  {
    return new Competition(10, COMPETITION_NAME, COMPETITION_DESC, 0, 0, 0, 0,
                           0, 0, 0, 0, null, 0, 0, 0, 0);
  }

  public static Competition spawnRandomCompetition ()
  {
    return new Competition(10, COMPETITION_NAME + Math.random(),
                           COMPETITION_DESC, 0, 0, 0, 0, 0, 0, 0, 0, null, 0, 0,
                           0, 0);
  }

  public static Customer spawnCustomerWithGenericPowerType (Competition competition)
  {
    return new Customer(TestHelper.CUSTOMER_POWERTAC_ID,
                        TestHelper.CUSTOMER_NAME, 50, "", 0, 0, 0, 0, false,
                        false, TestHelper.GENERIC_POWER_TYPE, competition);
  }

  public static Customer spawnRandomCustomer (Competition competition)
  {
    return new Customer(TestHelper.CUSTOMER_POWERTAC_ID,
                        TestHelper.CUSTOMER_NAME + Math.random(), 50, "", 0, 0,
                        0, 0, false, false,
                        Math.random() < 0.5f? TestHelper.GENERIC_POWER_TYPE
                                            : TestHelper.GENERIC_POWER_TYPE2,
                        competition);
  }

  public static Broker spawnRandomBroker (Competition comp)
  {
    return new Broker("test-broker"+Math.random(), comp);
  }

}
