"use strict";

 angular.module('config', [])

.constant('ENV', {name:'production',socketURL:'chat',baseRestEndpoint:'rest',retrieveRecentGameTicksEndpoint:'api/retrieve-ticks-from-most-recent-game',retrieveLastControl:'api/retrieve-last-control'})

;