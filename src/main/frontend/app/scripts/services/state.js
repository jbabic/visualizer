angular.module("sbAdminApp.services").service("state", function($filter, $log, websocketService) {
	'use strict';
	
	var state = this;
	
	
	
	state.init = function(){
		
		state.messages = [];
		
		// STATS START ----------------
		state.stats = {};
		
		state.stats.tariffStat = {
				number: "8",
				comments: "Number of Active Tariffs",
				colour: "primary",
				type: "comments",			
				goto: "dashboard.chart"
		};

		state.stats.wholsaleStat = {
				number: "12",
				comments: "Average Wholesale Price",
				colour: "primary",
				type: "support",			
				goto: "login"
		};
		
		state.stats.customerStat = {
				number: "18",
				comments: "Most Lucrative Customer Group",
				colour: "yellow",
				type: "shopping-cart",			
				goto: "login"
		};
		
		state.stats.brokerStat = {
				number: "24",
				comments: "Winning Broker",
				colour: "red",
				type: "user",			
				goto: "login"
		};
		// STATS END -----------------
		
		// TIMELINE START ----------------
		state.timeline = {};
		state.timeline.entries = [];
		
		
		var msg1 = {
			title : "Game started",
			badge : 'success',
			type : 'check',
			time : "1404781200000",
			text : "This is a title."
		};
		var msg2 = {
				title : "Something happened!",
				badge : 'danger',
				type : 'credit-card',
				time : "1404792000000",
				text : "This is a title for a second one."
			};
		var msg3 = {
				title : "Bomberman!",
				badge : 'danger',
				type : 'bomb',
				time : "1405278000000",
				text : "Oh my!"
			};
		var msg4 = {
				title : "Something happened!",
				badge : 'warning',
				type : 'credit-card',
				time : "1405458000000",
				text : "Nice text."
			};
		
		state.timeline.entries.unshift(msg1);
		state.timeline.entries.unshift(msg2);
		state.timeline.entries.unshift(msg3);
		state.timeline.entries.unshift(msg4);
		
		state.timeline.addEntryToTimeline = function(title, badge, type, time, text){
			state.timeline.entries.unshift({
				title: title,
				badge: badge,
				type: type,
				time: time,
				text: text
			});
		}
		// TIMELINE END ----------------
		
		// GAME STATS START -------------
		// We use an array to keep desired ordering:
		state.gameFacts = [];

		state.gameFacts.push({
			fact: 'gameName',
			type: 'gamepad',
			title: 'Game Name',
			text: 'game 69'
		});
		state.gameFacts.push({
				fact: 'gameStatus',
				type: 'stop',
				title: 'Status',
				text: 'Stopped'
			});
		state.gameFacts.push({
				fact: 'time',
				type: 'clock-o',
				title: 'Time',
				text: '00:00'
			});
		state.gameFacts.push({
				fact: 'brokerCount',
				type: 'users',
				title: 'Brokers',
				text: '8'
			});
		state.gameFacts.push({
			fact: 'populationCount',
			type: 'users',
			title: 'Population',
			text: $filter('number')("50000")
		});
		
		
		
		
		// build lookup object for easier access:
		state.gameFacts.lookup = {};
		for (var i = 0, len = state.gameFacts.length; i < len; i++) {
			state.gameFacts.lookup[state.gameFacts[i].fact] = state.gameFacts[i];
		}
		
		state.gameFacts.update = function(key, text){
			state.gameFacts.lookup[key].text = text;
		};
		state.gameFacts.updateTypeAndText = function(key, type, text){
			state.gameFacts.lookup[key].type = type;
			state.gameFacts.lookup[key].text = text;
		};
		// GAME STATS END ---------------
		
		
		// COMPETITION START ----------------
	
		
		// COMPETITION END ------------------
		
	};
	
	
	
		
	state.init();
	


	

});