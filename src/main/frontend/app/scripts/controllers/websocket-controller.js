'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:VisualizerChartCtrl
 * @description # Websocket Controller of the sbAdminApp
 */
angular.module("sbAdminApp.controllers").controller("WebsocketController", function($scope, websocketService, state, $log) {
 // see MainCtrl
	  
	  $scope.message = "";
	  $scope.max = 140;

	  $scope.addMessage = function() {
		  websocketService.send($scope.message);
	    $scope.message = "";
	  };

});