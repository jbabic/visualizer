'use strict';

angular.module("sbAdminApp.controllers").controller("GameManagerCtrl",
		function($scope, state, $log) {

			$scope.data = {
				availableOptions : [ {
					id : '1',
					name : 'Bootstrap Game'
				}, {
					id : '2',
					name : 'Simulation Game'
				} ],
				selectedOption : {
					id : '2',
					name : 'Simulation Game'
				}
			// This sets the default value of the select in the ui
			};

		});