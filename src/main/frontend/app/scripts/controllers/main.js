'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description # MainCtrl Controller of the sbAdminApp
 */
angular.module('sbAdminApp').controller(
		'MainCtrl',
		function($filter, $scope, $position, state, $log, websocketService, $http, ENV) {

			// store a service 'state' in MainCtrl's scope so the rest of the
			// app can use it
			$scope.state = state;
			
			
			$http.get(ENV.retrieveLastControl).success(function(data,status,headers,config){
				$log.info("LAST CONTROL:")
				$log.info(data);
				
				// catch up with the current game (i.e., retrieve 'ticks' in case a user arrived after the game has started)
				$http.get(ENV.retrieveRecentGameTicksEndpoint).success(function(data, status, headers, config){
					var messagesBeforeCatchUpCount = state.messages.length;
					var catchedUpMessagesCount = data.length;
					state.messages = _.union(data, state.messages);
					var resultingMessagesCount = state.messages.length;
					$log.info("Messages before catch up: "+messagesBeforeCatchUpCount + " Catched up messages: "+catchedUpMessagesCount+" Aggregated:"+resultingMessagesCount);	
				});
				
			});

			
			

			
		    // messageHandling
			websocketService.receive().then(null, null, function(message) {
			    state.messages.push(message);
			    $log.info(message);
			  });
			
			$scope.printMessages = function(){
				$log.info("MSG PRINT\n");
				$log.info(state.messages);
			};
		
			

			$scope.addRandomEntryTimeline = function() {
				var entry = {
					title : "THIS IS SPARTA",
					badge : 'warning',
					type : 'bomb',
					time : new Date(),
					text : "This is a title."
				};
				state.timeline.addEntryToTimeline("This is sparta", 'warning',
						'bomb', new Date(), "A Text...");
			};

			$scope.updateGameFactsTime = function() {
				state.gameFacts.update('time', $filter('date')(new Date(),
						'short'));
			}

		});
