
# VISUALIZER README
1. Make sure you have [bower](http://bower.io/), [grunt-cli](https://www.npmjs.com/package/grunt-cli) and  [npm](https://www.npmjs.org/) installed globally
2. On the command prompt run the following commands (from this directory)
- `npm install` - bower install is ran from the postinstall. This will install all the front-end dependencies such as JQuery and plotting libraries.
3. Choose what you want to do:
- [PROTOTYPE MODE] `grunt serve`, used for running stand-alone AngularJS front-end, useful for real-time prototyping without Spring back-end. 
- [DEPLOY MODE] `grunt build` to minify the files for deployment. Used to deploy the front-end to an appropriate (resources/static) folder.


#OLD (ORIGINAL) README
[SB Admin v2.0 angular](https://github.com/start-angular/sb-admin-angular)

## SB Admin v2.0 rewritten in AngularJS

[![Join the chat at https://gitter.im/start-angular/sb-admin-angular](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/start-angular/sb-admin-angular?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

This project is a port of the famous Free Admin Bootstrap Theme [SB Admin v2.0](http://startbootstrap.com/template-overviews/sb-admin-2/) to Angular Theme.

Find out more [Free Angular Themes at StartAngular.com](http://www.startangular.com/).

## Installation
1. Clone this project or Download that ZIP file
2. Make sure you have [bower](http://bower.io/), [grunt-cli](https://www.npmjs.com/package/grunt-cli) and  [npm](https://www.npmjs.org/) installed globally
3. On the command prompt run the following commands
- cd `project-directory`
- `npm install` - bower install is ran from the postinstall
- `npm start` - a shortcut for `grunt serve`
- `npm run dist` - a shortcut for `grunt serve:dist` to minify the files for deployment

## Roadmap

- Add sample AJAX calls and make the directives more modular

### Automation tools

- [Grunt](http://gruntjs.com/)
