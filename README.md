# Power TAC Visualizer

Research mode:
1. (First time only) Download and install local npm and node: `mvn com.github.eirslett:frontend-maven-plugin:install-node-and-npm -DnpmVersion=1.4.9 -DnodeVersion=v0.12.7 -DinstallDirectory=target`
2. (First time and every time you add new front-end dependencies in scr/main/resource/frontend/*.json) `mvn com.github.eirslett:frontend-maven-plugin:npm -DinstallDirectory=target -Darguments=install`
3. Run the app: `mvn spring-boot:run`
